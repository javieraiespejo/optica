<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facade\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Query\Builder;
Use App\Models\Cliente;

class ClienteController extends Controller
{
    public function crearCliente (){
        return view ('cliente.crearCliente');
    }

    public function store (Request $request){

        $nuevoC = new Cliente;
        
        $nuevoC->rut=$request->rut;
        $nuevoC->nombre=$request->nombre;
        $nuevoC->apellidoP=$request->apellidoP;
        $nuevoC->apellidoM=$request->apellidoM;
        $nuevoC->telefono=$request->telefono;
        $nuevoC->email=$request->email;
        $nuevoC->direccion=$request->direccion;
        $nuevoC->fecha=$request->fecha;
        

        $nuevoC->save();
        return redirect()->back()->with('mensaje','listo');
    }

  

}
