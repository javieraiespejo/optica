<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClienteController;


Route::get('cliente/crearCliente', [ClienteController::class, 'crearCliente'])->name('cliente.crearCliente');
Route::post('cliente', [ClienteController::class, 'store'])->name('cliente.store');

Route::get('/', function () {
    return view('welcome');
});
Route::get('app',function(){
    return view('layouts.app');
  });

  Route::get('/', function () {
    return view('welcome');
});
Route::get('/index',function(){
  return view('principal.index');
});
Route::get('/nuevoUsuario/',function(){
  return view('mantenedores.añadirUsuario');
});

Route::get('/cambiarContraseña/',function(){
  return view('mantenedores.cambiarContraseña');
});

Route::get('/eliminarUsuario/',function(){
  return view('mantenedores.eliminarUsuario');
});

Route::get('/ventasLentes/',function(){
  return view('ventas.lentesOpticos');
});

Route::get('/bazar/',function(){
  return view('ventas.bazar');
});

Route::get('/abonos/',function(){
  return view('reportes.abonos');
});

Route::get('/ingresos/',function(){
  return view('reportes.ingresos');
});

Route::get('/ingresos/',function(){
  return view('reportes.ingresos');
});


Route::get('/nuevoUsuario/',function(){
  return view('mantenedores.añadirUsuario');
});

Route::get('/cambiarContraseña/',function(){
  return view('mantenedores.cambiarContraseña');
});

Route::get('/eliminarUsuario/',function(){
  return view('mantenedores.eliminarUsuario');
});

Route::get('/ventasLentes/',function(){
  return view('ventas.lentesOpticos');
});

Route::get('/bazar/',function(){
  return view('ventas.bazar');
});

Route::get('/abonos/',function(){
  return view('reportes.abonos');
});

Route::get('/ingresos/',function(){
  return view('reportes.ingresos');
});

Route::get('/ingresos/',function(){
  return view('reportes.ingresos');
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

