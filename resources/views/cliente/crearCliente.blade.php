@extends('principal.index')

@section('contenido')

<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <h3 class="card-title text-muted template-demo">Nuevo Cliente</h3>
      <form action="{{route('cliente.store')}}" method='POST' class="form-sample">
       @csrf
        <p class="card-description"> Información Personal </p>

        <div class="row">

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nombre </label>
              <div class="col-sm-9">
                <input type="text"  name="nombre" id="nombre" class="form-control" />
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Apellido Paterno </label>
              <div class="col-sm-9">
                <input type="text" name="apellidoP" id="apellidoP" class="form-control" />
              </div>
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Apellido Materno </label>
              <div class="col-sm-9">
                <input type="text" name="apellidoM" id="apellidoM" class="form-control" />
              </div>
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Rut</label>
              <div class="col-sm-9">
                <input type="text" name="rut" id="rut" class="form-control"placeholder="11111111-1" />
              </div>
            </div>
          </div>

        </div>

        <div class="row">
        <!--  <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Sexo</label>
              <div class="col-sm-9">
                <select class="form-control">
                  <option>Masculino</option>
                  <option>Femenino</option>
                </select>
              </div>
            </div>
          </div>-->
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Fecha</label>
              <div class="col-sm-9">
                <input type="date" class="form-control" name="fecha" id="fecha" placeholder="dd/mm/yyyy" />
              </div>
            </div>
          </div>

        </div>

        <div class="row">

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Dirección</label>
              <div class="col-sm-9">
                <input type="text" name="direccion" id="direccion" class="form-control" />
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Teléfono</label>
              <div class="col-sm-9">
                <input type="text" name="telefono" id="telefono" class="form-control" />
              </div>
            </div>
          </div>

        </div>

        <div class="row">

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Email</label>
              <div class="col-sm-9">
                <input type="text" name="email" id="email" class="form-control" />
              </div>
            </div>
          </div>

        </div>

        <div class="nav justify-content-center">
          <button type="submit" class="btn btn-primary btn-sm mr-3">Guardar</button>
          <button class="btn btn-danger btn-sm mr-3">Cancelar</button>
        </div>
        
      </form>
    </div>
  </div>
</div>

@endsection

@section('scriptFooter')

@endsection
