

@extends('principal.index')

@section('contenido')

<div class="col-lg-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Bazar</h4>
      <!--<p class="card-description"> Add class <code>.table-striped</code> </p>-->
      <table class="table table-striped">
        <thead>
          <tr>
           <th>Nombre del producto</th>
            <th>Codigo del producto</th>
            <th>Precio del producto</th>
            <th>Editar producto</th>
            <th>Eliminar producto</th>
            
          </tr>
        </thead>
        <tbody>
          <tr>
            <td> -</td>
            <td> -</td>
            <td> -</td>
            <td> Editar </td>
            <td> Eliminar</td>
          </tr>
          <tr>
            <td> -</td>
            <td> -</td>
            <td> -</td>
            <td> Editar </td>
            <td> Eliminar</td>
          </tr>

        
        </tbody>
      </table>
    </div>
  </div>
</div>


@endsection


@section('scriptFooter')

@endsection