@extends('principal.index')

@section('contenido')

<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <form class="form-sample">
        <p class="card-description"><b> Información Lente de Cerca  </b></p>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Esférico ojo izquierdo </label>
              <div class="col-sm-4">
                <select class="form-control">
                  <option>+6.00</option>
                  <option>+5.75</option>
                  <option>+5.50</option>
                  <option>+5.25</option>
                  <option>+5.00</option>
                  <option>+4.75</option>
                  <option>+4.50</option>
                  <option>+4.25</option>
                  <option>+4.00</option>
                  <option>+3.75</option>
                  <option>+3.50</option>
                  <option>+3.25</option>
                  <option>+3.00</option>
                  <option>+2.75</option>
                  <option>+2.50</option>
                  <option>+2.25</option>
                  <option>+2.00</option>
                  <option>+1.75</option>
                  <option>+1.50</option>
                  <option>+1.25</option>
                  <option>+1.00</option>
                  <option>+0.75</option>
                  <option>+0.50</option>
                  <option>+0.25</option>
                  <option>+0.00</option>
                  <option>-0.25</option>
                  <option>-0.50</option>
                  <option>-0.75</option>
                  <option>-1.00</option>
                  <option>-0.25</option>
                  <option>-0.50</option>
                  <option>-0.75</option>
                  <option>-1.00</option>
                  <option>-1.25</option>
                  <option>-1.50</option>
                  <option>-1.75</option>
                  <option>-2.00</option>
                  <option>-2.25</option>
                  <option>-2.50</option>
                  <option>-2.75</option>
                  <option>-3.00</option>
                  <option>-3.25</option>
                  <option>-3.50</option>
                  <option>-3.75</option>
                  <option>-4.00</option>
                  <option>-4.25</option>
                  <option>-4.50</option>
                  <option>-4.75</option>
                  <option>-5.00</option>
                  <option>-5.25</option>
                  <option>-5.50</option>
                  <option>-5.75</option>
                  <option>-6.00</option>
                  <option>-6.25</option>
                  <option>-6.50</option>
                  <option>-6.75</option>
                  <option>-7.00</option>
                  <option>-7.25</option>
                  <option>-7.50</option>
                  <option>-7.75</option>
                  <option>-8.00</option>
                  <option>-8.25</option>
                  <option>-8.50</option>
                  <option>-8.75</option>
                  <option>-9.00</option>
                  <option>-9.25</option>
                  <option>-9.50</option>
                  <option>-9.75</option>
                  <option>-10.00</option>
                  <option>-10.25</option>
                  <option>-10.50</option>
                  <option>-10.75</option>
                  <option>-11.00</option>
                  <option>-11.25</option>
                  <option>-11.50</option>
                  <option>-11.75</option>
                  <option>-12.00</option>

                </select>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Esférico ojo derecho </label>
              <div class="col-sm-4">
                <select class="form-control">
                  <option>+6.00</option>
                  <option>+5.75</option>
                  <option>+5.50</option>
                  <option>+5.25</option>
                  <option>+5.00</option>
                  <option>+4.75</option>
                  <option>+4.50</option>
                  <option>+4.25</option>
                  <option>+4.00</option>
                  <option>+3.75</option>
                  <option>+3.50</option>
                  <option>+3.25</option>
                  <option>+3.00</option>
                  <option>+2.75</option>
                  <option>+2.50</option>
                  <option>+2.25</option>
                  <option>+2.00</option>
                  <option>+1.75</option>
                  <option>+1.50</option>
                  <option>+1.25</option>
                  <option>+1.00</option>
                  <option>+0.75</option>
                  <option>+0.50</option>
                  <option>+0.25</option>
                  <option>+0.00</option>
                  <option>-0.25</option>
                  <option>-0.50</option>
                  <option>-0.75</option>
                  <option>-1.00</option>
                  <option>-0.25</option>
                  <option>-0.50</option>
                  <option>-0.75</option>
                  <option>-1.00</option>
                  <option>-1.25</option>
                  <option>-1.50</option>
                  <option>-1.75</option>
                  <option>-2.00</option>
                  <option>-2.25</option>
                  <option>-2.50</option>
                  <option>-2.75</option>
                  <option>-3.00</option>
                  <option>-3.25</option>
                  <option>-3.50</option>
                  <option>-3.75</option>
                  <option>-4.00</option>
                  <option>-4.25</option>
                  <option>-4.50</option>
                  <option>-4.75</option>
                  <option>-5.00</option>
                  <option>-5.25</option>
                  <option>-5.50</option>
                  <option>-5.75</option>
                  <option>-6.00</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group row">
            <label class="col-sm-3 col-form-label">Cilíndrico ojo izquierdo </label>
            <div class="col-sm-4">
              <select class="form-control">
                <option>+6.00</option>
                <option>+5.75</option>
                <option>+5.50</option>
                <option>+5.25</option>
                <option>+5.00</option>
                <option>+4.75</option>
                <option>+4.50</option>
                <option>+4.25</option>
                <option>+4.00</option>
                <option>+3.75</option>
                <option>+3.50</option>
                <option>+3.25</option>
                <option>+3.00</option>
                <option>+2.75</option>
                <option>+2.50</option>
                <option>+2.25</option>
                <option>+2.00</option>
                <option>+1.75</option>
                <option>+1.50</option>
                <option>+1.25</option>
                <option>+1.00</option>
                <option>+0.75</option>
                <option>+0.50</option>
                <option>+0.25</option>
                <option>+0.00</option>
                <option>-0.25</option>
                <option>-0.50</option>
                <option>-0.75</option>
                <option>-1.00</option>
                <option>-0.25</option>
                <option>-0.50</option>
                <option>-0.75</option>
                <option>-1.00</option>
                <option>-1.25</option>
                <option>-1.50</option>
                <option>-1.75</option>
                <option>-2.00</option>
                <option>-2.25</option>
                <option>-2.50</option>
                <option>-2.75</option>
                <option>-3.00</option>
                <option>-3.25</option>
                <option>-3.50</option>
                <option>-3.75</option>
                <option>-4.00</option>
                <option>-4.25</option>
                <option>-4.50</option>
                <option>-4.75</option>
                <option>-5.00</option>
                <option>-5.25</option>
                <option>-5.50</option>
                <option>-5.75</option>
                <option>-6.00</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group row">
            <label class="col-sm-3 col-form-label">Cilíndrico ojo izquierdo </label>
            <div class="col-sm-4">
              <select class="form-control">
                <option>+6.00</option>
                <option>+5.75</option>
                <option>+5.50</option>
                <option>+5.25</option>
                <option>+5.00</option>
                <option>+4.75</option>
                <option>+4.50</option>
                <option>+4.25</option>
                <option>+4.00</option>
                <option>+3.75</option>
                <option>+3.50</option>
                <option>+3.25</option>
                <option>+3.00</option>
                <option>+2.75</option>
                <option>+2.50</option>
                <option>+2.25</option>
                <option>+2.00</option>
                <option>+1.75</option>
                <option>+1.50</option>
                <option>+1.25</option>
                <option>+1.00</option>
                <option>+0.75</option>
                <option>+0.50</option>
                <option>+0.25</option>
                <option>+0.00</option>
                <option>-0.25</option>
                <option>-0.50</option>
                <option>-0.75</option>
                <option>-1.00</option>
                <option>-0.25</option>
                <option>-0.50</option>
                <option>-0.75</option>
                <option>-1.00</option>
                <option>-1.25</option>
                <option>-1.50</option>
                <option>-1.75</option>
                <option>-2.00</option>
                <option>-2.25</option>
                <option>-2.50</option>
                <option>-2.75</option>
                <option>-3.00</option>
                <option>-3.25</option>
                <option>-3.50</option>
                <option>-3.75</option>
                <option>-4.00</option>
                <option>-4.25</option>
                <option>-4.50</option>
                <option>-4.75</option>
                <option>-5.00</option>
                <option>-5.25</option>
                <option>-5.50</option>
                <option>-5.75</option>
                <option>-6.00</option>
              </select>
            </div>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-6">
          <div class="form-group row">
              <label class="col-sm-3 col-form-label">Armazón </label>
          <div class="col-sm-4">
              <input type="text" class="form-control" placeholder="código"/>
          </div>
          </div>
        </div>
    </div>

          <div class="nav justify-content-right">
            <button class="btn btn-primary btn-sm mr-3" type="button" onclick="window.location='{{ route('/cotizar4') }}'">Siguiente</button>
            <button class="btn btn-danger btn-sm mr-3">Cancelar</button>
          </div>
          </div>
      </form>
    </div>
  </div>
</div>


@endsection

@section('scriptFooter')

@endsection
