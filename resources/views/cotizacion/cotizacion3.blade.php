@extends('principal.index')

@section('contenido')

<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
        <form class="form-sample">
        <p class="card-description"> Tipo de filtros </p>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nombre </label>
              <div class="col-sm-9">
                <input type="text" class="form-control" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Telefono</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" />
              </div>
            </div>
          </div>
        </div>
        
            </div>
          </div>
          </div>
 &nbsp;
          <div class="nav justify-content-right">
            <button  class="btn btn-primary btn-sm mr-3" type="button" onclick="window.location='{{ route('/cotizar2') }}'">Siguiente</button>
            <button  class="btn btn-danger btn-sm mr-3">Cancelar</button>
            
          </div>

      </form>
    </div>
  </div>
</div>
</div>
@endsection

@section('scriptFooter')

@endsection
