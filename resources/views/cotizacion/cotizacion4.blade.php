@extends('principal.index')

@section('contenido')
<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <form class="form-sample">
        <p class="card-description"><b> Información Financiera </b></p>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Valor Lente cerca </label>
              <div class="col-sm-4">
                <input type="text" class="form-control" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Valor lente Lejos</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" />
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Valor Total</label>
              <div class="col-sm-4">
                <input type="text" class="form-control"placeholder="Autosuma" />
              </div>
            </div>
          </div>
          <div class="col-md-6"> <div class="form-group row"></div> </div>
 &nbsp;
          <div class="nav justify-content-center">
            <button class="btn btn-primary btn-sm mr-3" type="button" onclick="window.location='{{ route('/cotizar3') }}'">Siguiente</button>
            <button class="btn btn-danger btn-sm mr-3">Cancelar</button>
          </div>
          </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('scriptFooter')

@endsection
