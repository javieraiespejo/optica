@extends('principal.index')

@section('contenido')

<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <form class="form-sample">
        <p class="card-description"><b> Información Lente de Lejos  </b></p>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                      <label class="col-sm-5 col-form-label">Observación laboratorio </label>
                        <div class="col-sm-8">
                            <textarea type="text" class="form-control" placeholder="Observación..."/> </textarea>
                        </div>
                  </div>
                </div>
              </div>
                <div class="nav justify-content-right">
                  <button type="submit" class="btn btn-primary btn-sm mr-3">Enviar</button>
                  <button class="btn btn-success  btn-sm mr-3">Imprimir</button>
                </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@section('scriptFooter')

@endsection
