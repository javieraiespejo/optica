@extends('principal.index')

@section('contenido')

<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <h3 class="card-title text-muted template-demo">Cotización Cliente Nuevo </h3>
      <form class="form-sample">
        <p class="card-description"> Información Personal Cliente </p>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Nombre </label>
              <div class="col-sm-9">
                <input type="text" class="form-control" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Telefono</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" />
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Numero de orden</label>
              <div class="col-sm-9">
                <input type="text" class="form-control"placeholder="Generar automaticamente" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Fecha hoy</label>
              <div class="col-sm-9">
                <input class="form-control" placeholder="dd/mm/yyyy" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Fecha Entrega</label>
              <div class="col-sm-9">
                <input class="form-control" placeholder="dd/mm/yyyy" />
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Tipo de Convenio</label>
            <div class="col-sm-4">
              <select class="form-control" placeholder="Seleccionar">
                <option>Local 1</option>
                <option>Local 2</option>
                <option>Local 3</option>
              </select>
            </div>
          </div>
          </div>
 &nbsp;
          <div class="nav justify-content-right">
            <button  class="btn btn-primary btn-sm mr-3" type="button" onclick="window.location='{{ route('/cotizar2') }}'">Siguiente</button>
            <button  class="btn btn-danger btn-sm mr-3">Cancelar</button>
            
          </div>

      </form>
    </div>
  </div>
</div>
</div>
@endsection

@section('scriptFooter')

@endsection
